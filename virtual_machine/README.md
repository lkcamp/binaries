# Virtual machine image

faime-L4PAWK44.qcow2 image was generated through https://fai-project.org/FAIme/cloud/ on 13 Aug 2018

Reference command:

    fai-diskimage -v -S7G -u debian -cAMD64,DEBIAN,DHCPC,DEMO,GRUB_PC,FAIBASE,CLOUD,STRETCH,SSH_SERVER,STANDARD,NONFREE,FAIME faime-L4PAWK44.qcow2
